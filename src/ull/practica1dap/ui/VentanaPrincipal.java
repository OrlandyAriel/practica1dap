package ull.practica1dap.ui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import ull.practica1dap.figuras.Circulo;
import ull.practica1dap.figuras.Cuadrado;
import ull.practica1dap.figuras.Figura;
import ull.practica1dap.figuras.Triangulo;
/**
 * @author Orlandy Ariel S�nchez A.
 * Pr�ctica 1, DAP
 */
public class VentanaPrincipal
{
	//ATRIBUTOS
	private JFrame mVentana;
	private JPanel mPanelFiguras;
	private JPanel mPanelBotones;
	private JButton mBtnArea;
	private JButton mBtnVolumen;

	private JRadioButton mRBCirculo;
	private JRadioButton mRBTriangulo;
	private JRadioButton mRBCuadrado;

	private ButtonGroup mBGrupoRadio;

	private CirculoDialog mCirculoDialog;
	private CuadradoDialog mCuadradoDialog;
	private TrianguloDialog mTrianguloDialog;
	
	private Figura mFigura;
	//CONSTRUCTOR/ES
	public VentanaPrincipal()
	{
		initComponent();
	}
	//M�TODOS Y FUNCIONES
	private void initComponent()
	{
		mBGrupoRadio = new ButtonGroup();
		
		mVentana = new JFrame("Pr�ctica 1- DAP");
		mVentana.setSize(500, 300);
		mVentana.setLocation((int) ((new Dimension().getWidth()/2)+500), 50);
		mVentana.setLayout(new BoxLayout(mVentana.getContentPane(),BoxLayout.Y_AXIS));
		mVentana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panelFiguras();
		panelBotones();
		mVentana.setVisible(true);
		mVentana.add(mPanelFiguras);
		mVentana.add(mPanelBotones);
	}
	private void panelBotones()
	{
		
		mPanelBotones = new JPanel();
		mPanelBotones.setLayout(new FlowLayout());
		
		mBtnArea = new JButton("�rea");
		mBtnArea.setVisible(true);
		mBtnArea.setEnabled(false);
		mBtnArea.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				seleccion();
				JOptionPane.showMessageDialog(mVentana, "�rea: "+mFigura.fArea());
			}
		});
		
		mBtnVolumen = new JButton("Volumen");
		mBtnVolumen.setVisible(true);
		mBtnVolumen.setEnabled(false);
		mBtnVolumen.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				seleccion();
				JOptionPane.showMessageDialog(mVentana, "Volumen: "+mFigura.fVolumen());
			}
		});
		mPanelBotones.add(mBtnArea);
		mPanelBotones.add(mBtnVolumen);
	}
	private void panelFiguras()
	{
		mPanelFiguras = new JPanel();
		mPanelFiguras.setLayout(new FlowLayout());
		mPanelFiguras.setVisible(true);

		mRBCirculo = new JRadioButton("C�rculo");
		mRBCirculo.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/recursos/circulo.png")));
		mRBCirculo.setVisible(true);
		mRBCirculo.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						mCirculoDialog = new CirculoDialog(mVentana, "Datos del C�rculo", true);
						dialogoVisible(mCirculoDialog);
					}
				}
		);
		mRBCuadrado = new JRadioButton("Cuadrado");
		mRBCuadrado.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/recursos/cuadrado.png")));
		mRBCuadrado.setVisible(true);
		mRBCuadrado.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						mCuadradoDialog = new CuadradoDialog( mVentana,"Datos del Cuadrado", true);
						dialogoVisible(mCuadradoDialog);
					}
				}
		);
		mRBTriangulo = new JRadioButton("Tri�ngulo");
		mRBTriangulo.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/recursos/triangulo.png")));
		mRBTriangulo.setVisible(true);
		mRBTriangulo.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						 mTrianguloDialog = new TrianguloDialog(mVentana, "Datos del Tri�ngulo", true);
						dialogoVisible(mTrianguloDialog);
					}
				}
		);
		mBGrupoRadio.add(mRBCirculo);
		mBGrupoRadio.add(mRBCuadrado);
		mBGrupoRadio.add(mRBTriangulo);
		
		mPanelFiguras.add(mRBCirculo);
		mPanelFiguras.add(mRBCuadrado);
		mPanelFiguras.add(mRBTriangulo); 
	}
	private void seleccion()
	{
		if(mRBCirculo.isSelected())
		{
			mFigura = new Circulo(mCirculoDialog.getMRadio());
		}else if(mRBCuadrado.isSelected())
		{
			mFigura = new Cuadrado(mCuadradoDialog.getMLado());
		}
		else if(mRBTriangulo.isSelected())
		{
			mFigura = new Triangulo(mTrianguloDialog.getMBase(),mTrianguloDialog.getMAltura());
		}
	}
	private void dialogoVisible(JDialog aDialog)
	{
		aDialog.setVisible(true);
		mBtnArea.setEnabled(true);
		mBtnVolumen.setEnabled(true);
	}
}
