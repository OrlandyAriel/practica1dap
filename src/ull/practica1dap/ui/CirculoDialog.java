package ull.practica1dap.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * @author Orlandy Ariel S�nchez A.
 * Pr�ctica 1, DAP
 */
public class CirculoDialog extends JDialog
{
	//ATRIBUTOS
	private JLabel mLabRadio;
	private JTextField mTxtRadio;
	private JButton mBtnEnviar;
	private String mRadio;
	//CONSTRUCTOR/ES
	public CirculoDialog(Frame aVentana, String aTitulo, boolean aModal)
	{
		super(aVentana, aTitulo,aModal);
		initComponent();
	}
	//M�TODOS Y FUNCIONES
	private void initComponent()
	{
		mLabRadio = new JLabel("Radio");
		mLabRadio.setVisible(true);
		
		mTxtRadio = new JTextField(5);
		
		mTxtRadio.setVisible(true);
		
		mBtnEnviar = new JButton("Enviar");
		mBtnEnviar.setVisible(true);
		mBtnEnviar.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if(!mTxtRadio.getText().isEmpty())
				{
					mRadio = mTxtRadio.getText();
					dispose();
					
				}else
				{
					JOptionPane.showMessageDialog(null, "Error,  no puedes dejar campos en blanco.","Mensaje de Error", JOptionPane.ERROR_MESSAGE); 

				}
			}
		});
		setLayout(new BorderLayout());
		setLocation((int) ((new Dimension().getWidth()/2)+600), 50);
		getContentPane().add(mLabRadio,BorderLayout.WEST);
		getContentPane().add(mTxtRadio, BorderLayout.EAST);
		getContentPane().add(mBtnEnviar, BorderLayout.SOUTH);
		pack();
		
	}
	public Double getMRadio()
	{
		return Double.parseDouble(mRadio);
	}
}
