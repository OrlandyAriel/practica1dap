package ull.practica1dap.figuras;

/**
 * @author Orlandy Ariel S�nchez A.
 * Pr�ctica 1, DAP
 */
public interface  Figura
{
	public  Double  fArea();
	public  Double fVolumen();
	public Double fPerimetro();
}
