package ull.practica1dap.figuras;

/**
 * @author Orlandy Ariel S�nchez A.
 * Pr�ctica 1, DAP
 */
public class Cuadrado implements Figura
{
	//ATRIBUTOS
	private Double mLado;
	//CONSTRUCTOR/ES & M�TODOS
	public Cuadrado()
	{
		this.mLado = 1D;
	}
	
	public Cuadrado(Double aLado)
	{
		this.mLado = aLado;
	}
	@Override
	public Double fArea()
	{
		return Math.pow(mLado, 2);
	}

	@Override
	public Double fVolumen()
	{
		return Math.pow(this.mLado, 3);
	}
	@Override
	public Double fPerimetro()
	{//suma de los 4 lados
		return 	4*mLado;
	}
}
