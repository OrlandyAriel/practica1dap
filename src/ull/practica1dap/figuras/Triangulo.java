package ull.practica1dap.figuras;

/**
 * @author Orlandy Ariel S�nchez A.
 * Pr�ctica 1, DAP
 */
public class Triangulo implements Figura
{
	//ATRIBUTOS
	private Double mBase;
	private Double mAltura;
	//CONSTRUCTOR/ES & M�TODOS
	public Triangulo()
	{
		this.mBase = 1D;
		this.mAltura = 1D;
	}
	public Triangulo(Double aBase, Double aAltura)
	{
		this.mBase = aBase;
		this.mAltura = aAltura;
	}
	@Override
	public Double fArea()
	{
		return (this.mBase * this.mAltura) /2 ;
	}
	@Override
	public Double fVolumen()
	{
		return ((mBase * mBase) * mAltura)/3.0;
	}
	@Override
	public Double fPerimetro()
	{//3*L o a+b+c (en caso de 3 lados distintos)
		return 	3*mAltura;
	}
}
