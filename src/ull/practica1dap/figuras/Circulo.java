package ull.practica1dap.figuras;

/**
 * @author Orlandy Ariel S�nchez A.
 * Pr�ctica 1, DAP
 */
public class Circulo implements Figura
{
	//ATRIBUTOS
	private Double mRadio;
	private Double mArea;
	//CONSTRUCTOR/ES & M�TODOS
	public Circulo()
	{
		this(1D);
	}
	public Circulo(Double aRadio)
	{
		this.mRadio = aRadio;
		this.mArea =0D;
	}
	
	public Double getMRadio()
	{
		return mRadio;
	}
	public void setMRadio(Double aRadio)
	{
		this.mRadio = aRadio;
	}
	@Override
	public Double fArea()
	{
		 this.mArea = Math.PI * Math.pow(mRadio, 2.0);
		 return this.mArea;
	}
	@Override
	public Double fVolumen()
	{
		return ((double)4/3 * Math.PI)*(Math.pow(mRadio, 3.0)) ;
	}
	@Override
	public Double fPerimetro()
	{//2pi*radio
		return 	2*Math.PI*mRadio;
	}

}
